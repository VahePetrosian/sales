﻿namespace SalesWinForm.Forms.Cashier
{
    partial class CashierMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.productsListGridView = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.totalSumLabel = new System.Windows.Forms.Label();
            this.totalSumLbl = new System.Windows.Forms.Label();
            this.totalPositionsCountLabel = new System.Windows.Forms.Label();
            this.totalPositionsCountLbl = new System.Windows.Forms.Label();
            this.docIdLabel = new System.Windows.Forms.Label();
            this.docIdLbl = new System.Windows.Forms.Label();
            this.docTypeLabel = new System.Windows.Forms.Label();
            this.docTypeLbl = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.searchResultGridView = new System.Windows.Forms.DataGridView();
            this.searchItemsPanel = new System.Windows.Forms.Panel();
            this.searchProductLbl = new System.Windows.Forms.Label();
            this.addProductCountLbl = new System.Windows.Forms.Label();
            this.addProductToCartButton = new System.Windows.Forms.Button();
            this.productCountTextBox = new System.Windows.Forms.TextBox();
            this.searchProductTextBox = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cashTextBox = new System.Windows.Forms.TextBox();
            this.changeLabel = new System.Windows.Forms.Label();
            this.changeLbl = new System.Windows.Forms.Label();
            this.cashLbl = new System.Windows.Forms.Label();
            this.CloseCartButton = new System.Windows.Forms.Button();
            this.positionDescriptionLabel = new System.Windows.Forms.Label();
            this.priceLbl = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.countLbl = new System.Windows.Forms.Label();
            this.countLabel = new System.Windows.Forms.Label();
            this.sumLbl = new System.Windows.Forms.Label();
            this.sumLabel = new System.Windows.Forms.Label();
            this.positionNameLabel = new System.Windows.Forms.Label();
            this.codeLabel = new System.Windows.Forms.Label();
            this.codeLbl = new System.Windows.Forms.Label();
            this.ClearAllButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.productsListGridView)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchResultGridView)).BeginInit();
            this.searchItemsPanel.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // productsListGridView
            // 
            this.productsListGridView.AllowUserToAddRows = false;
            this.productsListGridView.AllowUserToDeleteRows = false;
            this.productsListGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productsListGridView.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.productsListGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productsListGridView.Location = new System.Drawing.Point(0, 0);
            this.productsListGridView.Name = "productsListGridView";
            this.productsListGridView.ReadOnly = true;
            this.productsListGridView.Size = new System.Drawing.Size(800, 474);
            this.productsListGridView.TabIndex = 0;
            this.productsListGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.productsListGridView_CellContentClick);
            this.productsListGridView.Click += new System.EventHandler(this.productsListGridView_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.totalSumLabel);
            this.panel1.Controls.Add(this.totalSumLbl);
            this.panel1.Controls.Add(this.totalPositionsCountLabel);
            this.panel1.Controls.Add(this.totalPositionsCountLbl);
            this.panel1.Controls.Add(this.docIdLabel);
            this.panel1.Controls.Add(this.docIdLbl);
            this.panel1.Controls.Add(this.docTypeLabel);
            this.panel1.Controls.Add(this.docTypeLbl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 386);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 88);
            this.panel1.TabIndex = 1;
            // 
            // totalSumLabel
            // 
            this.totalSumLabel.AutoSize = true;
            this.totalSumLabel.Location = new System.Drawing.Point(577, 36);
            this.totalSumLabel.Name = "totalSumLabel";
            this.totalSumLabel.Size = new System.Drawing.Size(10, 13);
            this.totalSumLabel.TabIndex = 10;
            this.totalSumLabel.Text = "-";
            // 
            // totalSumLbl
            // 
            this.totalSumLbl.AutoSize = true;
            this.totalSumLbl.Location = new System.Drawing.Point(546, 12);
            this.totalSumLbl.Name = "totalSumLbl";
            this.totalSumLbl.Size = new System.Drawing.Size(98, 13);
            this.totalSumLbl.TabIndex = 9;
            this.totalSumLbl.Text = "Сумма документа";
            // 
            // totalPositionsCountLabel
            // 
            this.totalPositionsCountLabel.AutoSize = true;
            this.totalPositionsCountLabel.Location = new System.Drawing.Point(102, 66);
            this.totalPositionsCountLabel.Name = "totalPositionsCountLabel";
            this.totalPositionsCountLabel.Size = new System.Drawing.Size(10, 13);
            this.totalPositionsCountLabel.TabIndex = 8;
            this.totalPositionsCountLabel.Text = "-";
            // 
            // totalPositionsCountLbl
            // 
            this.totalPositionsCountLbl.AutoSize = true;
            this.totalPositionsCountLbl.Location = new System.Drawing.Point(8, 66);
            this.totalPositionsCountLbl.Name = "totalPositionsCountLbl";
            this.totalPositionsCountLbl.Size = new System.Drawing.Size(51, 13);
            this.totalPositionsCountLbl.TabIndex = 7;
            this.totalPositionsCountLbl.Text = "Позиций";
            // 
            // docIdLabel
            // 
            this.docIdLabel.AutoSize = true;
            this.docIdLabel.Location = new System.Drawing.Point(102, 36);
            this.docIdLabel.Name = "docIdLabel";
            this.docIdLabel.Size = new System.Drawing.Size(10, 13);
            this.docIdLabel.TabIndex = 6;
            this.docIdLabel.Text = "-";
            // 
            // docIdLbl
            // 
            this.docIdLbl.AutoSize = true;
            this.docIdLbl.Location = new System.Drawing.Point(8, 36);
            this.docIdLbl.Name = "docIdLbl";
            this.docIdLbl.Size = new System.Drawing.Size(78, 13);
            this.docIdLbl.TabIndex = 5;
            this.docIdLbl.Text = "№ Документа";
            // 
            // docTypeLabel
            // 
            this.docTypeLabel.AutoSize = true;
            this.docTypeLabel.Location = new System.Drawing.Point(102, 12);
            this.docTypeLabel.Name = "docTypeLabel";
            this.docTypeLabel.Size = new System.Drawing.Size(10, 13);
            this.docTypeLabel.TabIndex = 4;
            this.docTypeLabel.Text = "-";
            // 
            // docTypeLbl
            // 
            this.docTypeLbl.AutoSize = true;
            this.docTypeLbl.Location = new System.Drawing.Point(8, 12);
            this.docTypeLbl.Name = "docTypeLbl";
            this.docTypeLbl.Size = new System.Drawing.Size(83, 13);
            this.docTypeLbl.TabIndex = 3;
            this.docTypeLbl.Text = "Вид документа";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.searchItemsPanel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 253);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 133);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.searchResultGridView);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(341, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(459, 133);
            this.panel3.TabIndex = 3;
            // 
            // searchResultGridView
            // 
            this.searchResultGridView.AllowUserToAddRows = false;
            this.searchResultGridView.AllowUserToDeleteRows = false;
            this.searchResultGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.searchResultGridView.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.searchResultGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchResultGridView.Location = new System.Drawing.Point(0, 0);
            this.searchResultGridView.Name = "searchResultGridView";
            this.searchResultGridView.ReadOnly = true;
            this.searchResultGridView.Size = new System.Drawing.Size(459, 133);
            this.searchResultGridView.TabIndex = 2;
            // 
            // searchItemsPanel
            // 
            this.searchItemsPanel.Controls.Add(this.searchProductLbl);
            this.searchItemsPanel.Controls.Add(this.addProductCountLbl);
            this.searchItemsPanel.Controls.Add(this.addProductToCartButton);
            this.searchItemsPanel.Controls.Add(this.productCountTextBox);
            this.searchItemsPanel.Controls.Add(this.searchProductTextBox);
            this.searchItemsPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.searchItemsPanel.Location = new System.Drawing.Point(0, 0);
            this.searchItemsPanel.Name = "searchItemsPanel";
            this.searchItemsPanel.Size = new System.Drawing.Size(341, 133);
            this.searchItemsPanel.TabIndex = 2;
            // 
            // searchProductLbl
            // 
            this.searchProductLbl.AutoSize = true;
            this.searchProductLbl.Location = new System.Drawing.Point(3, 9);
            this.searchProductLbl.Name = "searchProductLbl";
            this.searchProductLbl.Size = new System.Drawing.Size(88, 13);
            this.searchProductLbl.TabIndex = 5;
            this.searchProductLbl.Text = "Поиск продукта";
            // 
            // addProductCountLbl
            // 
            this.addProductCountLbl.AutoSize = true;
            this.addProductCountLbl.Location = new System.Drawing.Point(3, 35);
            this.addProductCountLbl.Name = "addProductCountLbl";
            this.addProductCountLbl.Size = new System.Drawing.Size(66, 13);
            this.addProductCountLbl.TabIndex = 3;
            this.addProductCountLbl.Text = "Количество";
            // 
            // addProductToCartButton
            // 
            this.addProductToCartButton.Location = new System.Drawing.Point(105, 58);
            this.addProductToCartButton.Name = "addProductToCartButton";
            this.addProductToCartButton.Size = new System.Drawing.Size(213, 69);
            this.addProductToCartButton.TabIndex = 2;
            this.addProductToCartButton.Text = "Добавить в корзину";
            this.addProductToCartButton.UseVisualStyleBackColor = true;
            this.addProductToCartButton.Click += new System.EventHandler(this.addProductToCartButton_Click);
            // 
            // productCountTextBox
            // 
            this.productCountTextBox.Location = new System.Drawing.Point(105, 32);
            this.productCountTextBox.Name = "productCountTextBox";
            this.productCountTextBox.Size = new System.Drawing.Size(213, 20);
            this.productCountTextBox.TabIndex = 1;
            // 
            // searchProductTextBox
            // 
            this.searchProductTextBox.Location = new System.Drawing.Point(105, 6);
            this.searchProductTextBox.Name = "searchProductTextBox";
            this.searchProductTextBox.Size = new System.Drawing.Size(213, 20);
            this.searchProductTextBox.TabIndex = 0;
            this.searchProductTextBox.TextChanged += new System.EventHandler(this.searchProductTextBox_TextChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ClearAllButton);
            this.panel4.Controls.Add(this.cashTextBox);
            this.panel4.Controls.Add(this.changeLabel);
            this.panel4.Controls.Add(this.changeLbl);
            this.panel4.Controls.Add(this.cashLbl);
            this.panel4.Controls.Add(this.CloseCartButton);
            this.panel4.Controls.Add(this.positionDescriptionLabel);
            this.panel4.Controls.Add(this.priceLbl);
            this.panel4.Controls.Add(this.priceLabel);
            this.panel4.Controls.Add(this.countLbl);
            this.panel4.Controls.Add(this.countLabel);
            this.panel4.Controls.Add(this.sumLbl);
            this.panel4.Controls.Add(this.sumLabel);
            this.panel4.Controls.Add(this.positionNameLabel);
            this.panel4.Controls.Add(this.codeLabel);
            this.panel4.Controls.Add(this.codeLbl);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 174);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 79);
            this.panel4.TabIndex = 4;
            // 
            // cashTextBox
            // 
            this.cashTextBox.Location = new System.Drawing.Point(468, 23);
            this.cashTextBox.Name = "cashTextBox";
            this.cashTextBox.Size = new System.Drawing.Size(62, 20);
            this.cashTextBox.TabIndex = 14;
            // 
            // changeLabel
            // 
            this.changeLabel.AutoSize = true;
            this.changeLabel.Location = new System.Drawing.Point(470, 49);
            this.changeLabel.Name = "changeLabel";
            this.changeLabel.Size = new System.Drawing.Size(10, 13);
            this.changeLabel.TabIndex = 13;
            this.changeLabel.Text = "-";
            // 
            // changeLbl
            // 
            this.changeLbl.AutoSize = true;
            this.changeLbl.Location = new System.Drawing.Point(404, 49);
            this.changeLbl.Name = "changeLbl";
            this.changeLbl.Size = new System.Drawing.Size(37, 13);
            this.changeLbl.TabIndex = 12;
            this.changeLbl.Text = "Сдача";
            // 
            // cashLbl
            // 
            this.cashLbl.AutoSize = true;
            this.cashLbl.Location = new System.Drawing.Point(404, 26);
            this.cashLbl.Name = "cashLbl";
            this.cashLbl.Size = new System.Drawing.Size(58, 13);
            this.cashLbl.TabIndex = 11;
            this.cashLbl.Text = "Наличные";
            // 
            // CloseCartButton
            // 
            this.CloseCartButton.Enabled = false;
            this.CloseCartButton.Location = new System.Drawing.Point(580, 7);
            this.CloseCartButton.Name = "CloseCartButton";
            this.CloseCartButton.Size = new System.Drawing.Size(208, 23);
            this.CloseCartButton.TabIndex = 10;
            this.CloseCartButton.Text = "Готово";
            this.CloseCartButton.UseVisualStyleBackColor = true;
            this.CloseCartButton.Click += new System.EventHandler(this.CloseCartButton_Click);
            // 
            // positionDescriptionLabel
            // 
            this.positionDescriptionLabel.AutoSize = true;
            this.positionDescriptionLabel.Location = new System.Drawing.Point(228, 49);
            this.positionDescriptionLabel.Name = "positionDescriptionLabel";
            this.positionDescriptionLabel.Size = new System.Drawing.Size(10, 13);
            this.positionDescriptionLabel.TabIndex = 9;
            this.positionDescriptionLabel.Text = "-";
            // 
            // priceLbl
            // 
            this.priceLbl.AutoSize = true;
            this.priceLbl.Location = new System.Drawing.Point(149, 4);
            this.priceLbl.Name = "priceLbl";
            this.priceLbl.Size = new System.Drawing.Size(33, 13);
            this.priceLbl.TabIndex = 8;
            this.priceLbl.Text = "Цена";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(190, 4);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(10, 13);
            this.priceLabel.TabIndex = 7;
            this.priceLabel.Text = "-";
            // 
            // countLbl
            // 
            this.countLbl.AutoSize = true;
            this.countLbl.Location = new System.Drawing.Point(275, 4);
            this.countLbl.Name = "countLbl";
            this.countLbl.Size = new System.Drawing.Size(66, 13);
            this.countLbl.TabIndex = 6;
            this.countLbl.Text = "Количество";
            // 
            // countLabel
            // 
            this.countLabel.AutoSize = true;
            this.countLabel.Location = new System.Drawing.Point(347, 4);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(10, 13);
            this.countLabel.TabIndex = 5;
            this.countLabel.Text = "-";
            // 
            // sumLbl
            // 
            this.sumLbl.AutoSize = true;
            this.sumLbl.Location = new System.Drawing.Point(404, 4);
            this.sumLbl.Name = "sumLbl";
            this.sumLbl.Size = new System.Drawing.Size(41, 13);
            this.sumLbl.TabIndex = 4;
            this.sumLbl.Text = "Сумма";
            // 
            // sumLabel
            // 
            this.sumLabel.AutoSize = true;
            this.sumLabel.Location = new System.Drawing.Point(470, 4);
            this.sumLabel.Name = "sumLabel";
            this.sumLabel.Size = new System.Drawing.Size(10, 13);
            this.sumLabel.TabIndex = 3;
            this.sumLabel.Text = "-";
            // 
            // positionNameLabel
            // 
            this.positionNameLabel.AutoSize = true;
            this.positionNameLabel.Location = new System.Drawing.Point(13, 49);
            this.positionNameLabel.Name = "positionNameLabel";
            this.positionNameLabel.Size = new System.Drawing.Size(10, 13);
            this.positionNameLabel.TabIndex = 2;
            this.positionNameLabel.Text = "-";
            // 
            // codeLabel
            // 
            this.codeLabel.AutoSize = true;
            this.codeLabel.Location = new System.Drawing.Point(54, 4);
            this.codeLabel.Name = "codeLabel";
            this.codeLabel.Size = new System.Drawing.Size(10, 13);
            this.codeLabel.TabIndex = 1;
            this.codeLabel.Text = "-";
            // 
            // codeLbl
            // 
            this.codeLbl.AutoSize = true;
            this.codeLbl.Location = new System.Drawing.Point(13, 4);
            this.codeLbl.Name = "codeLbl";
            this.codeLbl.Size = new System.Drawing.Size(26, 13);
            this.codeLbl.TabIndex = 0;
            this.codeLbl.Text = "Код";
            // 
            // ClearAllButton
            // 
            this.ClearAllButton.Enabled = false;
            this.ClearAllButton.Location = new System.Drawing.Point(580, 36);
            this.ClearAllButton.Name = "ClearAllButton";
            this.ClearAllButton.Size = new System.Drawing.Size(208, 27);
            this.ClearAllButton.TabIndex = 15;
            this.ClearAllButton.Text = "Очистить";
            this.ClearAllButton.UseVisualStyleBackColor = true;
            this.ClearAllButton.Click += new System.EventHandler(this.clearAllButton_Click);
            // 
            // CashierMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 474);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.productsListGridView);
            this.Name = "CashierMainForm";
            this.Text = "Кассир";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CashierMainForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.productsListGridView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchResultGridView)).EndInit();
            this.searchItemsPanel.ResumeLayout(false);
            this.searchItemsPanel.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView productsListGridView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label totalSumLabel;
        private System.Windows.Forms.Label totalSumLbl;
        private System.Windows.Forms.Label totalPositionsCountLabel;
        private System.Windows.Forms.Label totalPositionsCountLbl;
        private System.Windows.Forms.Label docIdLabel;
        private System.Windows.Forms.Label docIdLbl;
        private System.Windows.Forms.Label docTypeLabel;
        private System.Windows.Forms.Label docTypeLbl;
        private System.Windows.Forms.Label positionDescriptionLabel;
        private System.Windows.Forms.Label priceLbl;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label countLbl;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.Label sumLbl;
        private System.Windows.Forms.Label sumLabel;
        private System.Windows.Forms.Label positionNameLabel;
        private System.Windows.Forms.Label codeLabel;
        private System.Windows.Forms.Label codeLbl;
        private System.Windows.Forms.Panel searchItemsPanel;
        private System.Windows.Forms.TextBox searchProductTextBox;
        private System.Windows.Forms.Label searchProductLbl;
        private System.Windows.Forms.Label addProductCountLbl;
        private System.Windows.Forms.Button addProductToCartButton;
        private System.Windows.Forms.TextBox productCountTextBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView searchResultGridView;
        private System.Windows.Forms.Button CloseCartButton;
        private System.Windows.Forms.TextBox cashTextBox;
        private System.Windows.Forms.Label changeLabel;
        private System.Windows.Forms.Label changeLbl;
        private System.Windows.Forms.Label cashLbl;
        private System.Windows.Forms.Button ClearAllButton;
    }
}