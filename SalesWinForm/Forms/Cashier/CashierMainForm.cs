﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SalesWinForm.Code;

namespace SalesWinForm.Forms.Cashier
{
    public partial class CashierMainForm : Form
    {
        List<CartDataModel> cartList;
        UserAuthData currentUserAuthData;

        private void UpdateCartGridView()
        {
            productsListGridView.Columns.Clear();
            productsListGridView.DataSource = CashierHandler.GetCardDataTable(cartList);

            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn();
            productsListGridView.Columns.Add(deleteButton);
            deleteButton.HeaderText = "Удалить";
            deleteButton.Text = "Удалить";
            deleteButton.Name = "deleteButton";
            deleteButton.UseColumnTextForButtonValue = true;
        }

        public CashierMainForm(UserAuthData userAuthData)
        {
            currentUserAuthData = userAuthData;
            cartList = new List<CartDataModel>();
            InitializeComponent();
        }

        private void searchProductTextBox_TextChanged(object sender, EventArgs e)
        {
            string searchValue = searchProductTextBox.Text;
            searchResultGridView.DataSource = CashierHandler.GetSearchResultDataTable(searchValue);
        }

        private void addProductToCartButton_Click(object sender, EventArgs e)
        {
            if (searchResultGridView.CurrentRow == null)
                return;
            DataGridViewRow row = searchResultGridView.Rows[searchResultGridView.CurrentRow.Index];
            int itemId = (int)row.Cells[0].Value;
            int count = 0;
            Int32.TryParse(productCountTextBox.Text, out count);
            if (itemId > 0 && count > 0)
            {
                CashierHandler.AddItemToCart(itemId, count, ref cartList);
                UpdateCartGridView();
                UpdatePageView();
                ClearAllButton.Enabled = true;
                CloseCartButton.Enabled = true;
            }
        }

        private void UpdatePageView()
        {
            if (productsListGridView.CurrentRow == null)
            {
                changeLabel.Text=
                codeLabel.Text =
                priceLabel.Text =
                countLabel.Text =
                sumLabel.Text =
                positionNameLabel.Text =
                positionDescriptionLabel.Text =
                totalPositionsCountLabel.Text =
                totalSumLabel.Text = "-";
                cashTextBox.Text = "";
                
                return;
            }

            DataGridViewRow row = productsListGridView.Rows[productsListGridView.CurrentRow.Index];
            int selectedItemId = (int)row.Cells[0].Value;
            CartDataModel selectedItemModel = cartList.FirstOrDefault(cartItem => cartItem.Id == selectedItemId);

            codeLabel.Text = selectedItemModel.Id.ToString();
            priceLabel.Text = selectedItemModel.Price.ToString("0.##");
            countLabel.Text = selectedItemModel.Count.ToString();
            sumLabel.Text = selectedItemModel.Sum.ToString("0.##");
            positionNameLabel.Text = selectedItemModel.Name;
            positionDescriptionLabel.Text = selectedItemModel.Description;

            totalPositionsCountLabel.Text = cartList.Count().ToString();
            totalSumLabel.Text = cartList.Sum(item => item.Sum).ToString("0.##");
        }

        private void productsListGridView_Click(object sender, EventArgs e)
        {
            UpdatePageView();
        }

        private void productsListGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (productsListGridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                if (productsListGridView.CurrentRow == null)
                    return;
                DataGridViewRow row = productsListGridView.Rows[productsListGridView.CurrentRow.Index];
                int selectedItemId = (int)row.Cells[0].Value;
                CashierHandler.RemoveItemFromCart(selectedItemId, ref cartList);
                UpdateCartGridView();
                UpdatePageView();
                if (cartList.Count == 0)
                {

                    ClearAllButton.Enabled = false;
                    CloseCartButton.Enabled = false;
                }
            }
        }

        private void CloseCartButton_Click(object sender, EventArgs e)
        {
            decimal cash = 0;
            decimal.TryParse(cashTextBox.Text, out cash);
            decimal total = cartList.Sum(item => item.Sum);
            if (cash >= total)
            {
                CashierHandler.SaveCartToDb(cartList, cash, total, currentUserAuthData.UserId);
                changeLabel.Text = (cash - total).ToString("0.##");
                CloseCartButton.Enabled = false;
                productsListGridView.Enabled = false;
                addProductToCartButton.Enabled = false;
                cashTextBox.Enabled = false;
            }
        }

        private void CashierMainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void clearAllButton_Click(object sender, EventArgs e)
        {
            cartList = new List<CartDataModel>();
            cashTextBox.Enabled = true;
            CloseCartButton.Enabled = true;
            productsListGridView.Enabled = true;
            addProductToCartButton.Enabled = true;
            UpdateCartGridView();
            string searchValue = searchProductTextBox.Text;
            searchResultGridView.DataSource = CashierHandler.GetSearchResultDataTable(searchValue);
            UpdatePageView();
        }
    }
}
