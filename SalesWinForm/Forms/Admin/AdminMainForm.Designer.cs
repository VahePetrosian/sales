﻿namespace SalesWinForm.Forms.Admin
{
    partial class AdminMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.usersListPage = new System.Windows.Forms.TabPage();
            this.usersGridView = new System.Windows.Forms.DataGridView();
            this.addUserPage = new System.Windows.Forms.TabPage();
            this.addNewUserButton = new System.Windows.Forms.Button();
            this.newUserChooseRoleLabel = new System.Windows.Forms.Label();
            this.newUserChooseRoleDropDown = new System.Windows.Forms.ComboBox();
            this.newUserPasswordTextBox = new System.Windows.Forms.TextBox();
            this.newUserSurnameTextBox = new System.Windows.Forms.TextBox();
            this.newUserNameTextBox = new System.Windows.Forms.TextBox();
            this.newUserLoginTextBox = new System.Windows.Forms.TextBox();
            this.newUserPasswordLabel = new System.Windows.Forms.Label();
            this.newUserSurnameLabel = new System.Windows.Forms.Label();
            this.newUserNameLabel = new System.Windows.Forms.Label();
            this.newUserLoginLabel = new System.Windows.Forms.Label();
            this.itemsListPage = new System.Windows.Forms.TabPage();
            this.allItemsGridView = new System.Windows.Forms.DataGridView();
            this.salesHistoryPage = new System.Windows.Forms.TabPage();
            this.saleItemsGridView = new System.Windows.Forms.DataGridView();
            this.salesHistoryGridView = new System.Windows.Forms.DataGridView();
            this.incomeHistoryPage = new System.Windows.Forms.TabPage();
            this.incomeHistoryGridView = new System.Windows.Forms.DataGridView();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1.SuspendLayout();
            this.usersListPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersGridView)).BeginInit();
            this.addUserPage.SuspendLayout();
            this.itemsListPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allItemsGridView)).BeginInit();
            this.salesHistoryPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saleItemsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesHistoryGridView)).BeginInit();
            this.incomeHistoryPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incomeHistoryGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.usersListPage);
            this.tabControl1.Controls.Add(this.addUserPage);
            this.tabControl1.Controls.Add(this.itemsListPage);
            this.tabControl1.Controls.Add(this.salesHistoryPage);
            this.tabControl1.Controls.Add(this.incomeHistoryPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(885, 528);
            this.tabControl1.TabIndex = 0;
            // 
            // usersListPage
            // 
            this.usersListPage.Controls.Add(this.usersGridView);
            this.usersListPage.Location = new System.Drawing.Point(4, 22);
            this.usersListPage.Name = "usersListPage";
            this.usersListPage.Padding = new System.Windows.Forms.Padding(3);
            this.usersListPage.Size = new System.Drawing.Size(877, 502);
            this.usersListPage.TabIndex = 0;
            this.usersListPage.Text = "Пользователи";
            this.usersListPage.UseVisualStyleBackColor = true;
            // 
            // usersGridView
            // 
            this.usersGridView.AllowUserToAddRows = false;
            this.usersGridView.AllowUserToDeleteRows = false;
            this.usersGridView.AllowUserToResizeColumns = false;
            this.usersGridView.AllowUserToResizeRows = false;
            this.usersGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.usersGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.usersGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usersGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.usersGridView.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.usersGridView.Location = new System.Drawing.Point(3, 3);
            this.usersGridView.MultiSelect = false;
            this.usersGridView.Name = "usersGridView";
            this.usersGridView.ReadOnly = true;
            this.usersGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.usersGridView.Size = new System.Drawing.Size(871, 496);
            this.usersGridView.TabIndex = 0;
            // 
            // addUserPage
            // 
            this.addUserPage.Controls.Add(this.addNewUserButton);
            this.addUserPage.Controls.Add(this.newUserChooseRoleLabel);
            this.addUserPage.Controls.Add(this.newUserChooseRoleDropDown);
            this.addUserPage.Controls.Add(this.newUserPasswordTextBox);
            this.addUserPage.Controls.Add(this.newUserSurnameTextBox);
            this.addUserPage.Controls.Add(this.newUserNameTextBox);
            this.addUserPage.Controls.Add(this.newUserLoginTextBox);
            this.addUserPage.Controls.Add(this.newUserPasswordLabel);
            this.addUserPage.Controls.Add(this.newUserSurnameLabel);
            this.addUserPage.Controls.Add(this.newUserNameLabel);
            this.addUserPage.Controls.Add(this.newUserLoginLabel);
            this.addUserPage.Location = new System.Drawing.Point(4, 22);
            this.addUserPage.Name = "addUserPage";
            this.addUserPage.Padding = new System.Windows.Forms.Padding(3);
            this.addUserPage.Size = new System.Drawing.Size(877, 502);
            this.addUserPage.TabIndex = 1;
            this.addUserPage.Text = "Новый пользователь";
            this.addUserPage.UseVisualStyleBackColor = true;
            // 
            // addNewUserButton
            // 
            this.addNewUserButton.Location = new System.Drawing.Point(47, 170);
            this.addNewUserButton.Name = "addNewUserButton";
            this.addNewUserButton.Size = new System.Drawing.Size(191, 23);
            this.addNewUserButton.TabIndex = 10;
            this.addNewUserButton.Text = "Создать";
            this.addNewUserButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.addNewUserButton.UseVisualStyleBackColor = true;
            this.addNewUserButton.Click += new System.EventHandler(this.addNewUserButton_Click);
            // 
            // newUserChooseRoleLabel
            // 
            this.newUserChooseRoleLabel.AutoSize = true;
            this.newUserChooseRoleLabel.Location = new System.Drawing.Point(44, 145);
            this.newUserChooseRoleLabel.Name = "newUserChooseRoleLabel";
            this.newUserChooseRoleLabel.Size = new System.Drawing.Size(32, 13);
            this.newUserChooseRoleLabel.TabIndex = 9;
            this.newUserChooseRoleLabel.Text = "Роль";
            // 
            // newUserChooseRoleDropDown
            // 
            this.newUserChooseRoleDropDown.FormattingEnabled = true;
            this.newUserChooseRoleDropDown.Location = new System.Drawing.Point(117, 142);
            this.newUserChooseRoleDropDown.Name = "newUserChooseRoleDropDown";
            this.newUserChooseRoleDropDown.Size = new System.Drawing.Size(121, 21);
            this.newUserChooseRoleDropDown.TabIndex = 8;
            // 
            // newUserPasswordTextBox
            // 
            this.newUserPasswordTextBox.Location = new System.Drawing.Point(117, 115);
            this.newUserPasswordTextBox.Name = "newUserPasswordTextBox";
            this.newUserPasswordTextBox.PasswordChar = '*';
            this.newUserPasswordTextBox.Size = new System.Drawing.Size(121, 20);
            this.newUserPasswordTextBox.TabIndex = 7;
            // 
            // newUserSurnameTextBox
            // 
            this.newUserSurnameTextBox.Location = new System.Drawing.Point(117, 89);
            this.newUserSurnameTextBox.Name = "newUserSurnameTextBox";
            this.newUserSurnameTextBox.Size = new System.Drawing.Size(121, 20);
            this.newUserSurnameTextBox.TabIndex = 6;
            // 
            // newUserNameTextBox
            // 
            this.newUserNameTextBox.Location = new System.Drawing.Point(117, 63);
            this.newUserNameTextBox.Name = "newUserNameTextBox";
            this.newUserNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.newUserNameTextBox.TabIndex = 5;
            // 
            // newUserLoginTextBox
            // 
            this.newUserLoginTextBox.Location = new System.Drawing.Point(117, 37);
            this.newUserLoginTextBox.Name = "newUserLoginTextBox";
            this.newUserLoginTextBox.Size = new System.Drawing.Size(121, 20);
            this.newUserLoginTextBox.TabIndex = 4;
            // 
            // newUserPasswordLabel
            // 
            this.newUserPasswordLabel.AutoSize = true;
            this.newUserPasswordLabel.Location = new System.Drawing.Point(41, 118);
            this.newUserPasswordLabel.Name = "newUserPasswordLabel";
            this.newUserPasswordLabel.Size = new System.Drawing.Size(45, 13);
            this.newUserPasswordLabel.TabIndex = 3;
            this.newUserPasswordLabel.Text = "Пароль";
            // 
            // newUserSurnameLabel
            // 
            this.newUserSurnameLabel.AutoSize = true;
            this.newUserSurnameLabel.Location = new System.Drawing.Point(41, 92);
            this.newUserSurnameLabel.Name = "newUserSurnameLabel";
            this.newUserSurnameLabel.Size = new System.Drawing.Size(56, 13);
            this.newUserSurnameLabel.TabIndex = 2;
            this.newUserSurnameLabel.Text = "Фамилия";
            // 
            // newUserNameLabel
            // 
            this.newUserNameLabel.AutoSize = true;
            this.newUserNameLabel.Location = new System.Drawing.Point(41, 66);
            this.newUserNameLabel.Name = "newUserNameLabel";
            this.newUserNameLabel.Size = new System.Drawing.Size(29, 13);
            this.newUserNameLabel.TabIndex = 1;
            this.newUserNameLabel.Text = "Имя";
            // 
            // newUserLoginLabel
            // 
            this.newUserLoginLabel.AutoSize = true;
            this.newUserLoginLabel.Location = new System.Drawing.Point(41, 40);
            this.newUserLoginLabel.Name = "newUserLoginLabel";
            this.newUserLoginLabel.Size = new System.Drawing.Size(38, 13);
            this.newUserLoginLabel.TabIndex = 0;
            this.newUserLoginLabel.Text = "Логин";
            // 
            // itemsListPage
            // 
            this.itemsListPage.Controls.Add(this.allItemsGridView);
            this.itemsListPage.Location = new System.Drawing.Point(4, 22);
            this.itemsListPage.Name = "itemsListPage";
            this.itemsListPage.Size = new System.Drawing.Size(877, 502);
            this.itemsListPage.TabIndex = 2;
            this.itemsListPage.Text = "Товары";
            this.itemsListPage.UseVisualStyleBackColor = true;
            // 
            // allItemsGridView
            // 
            this.allItemsGridView.AllowUserToAddRows = false;
            this.allItemsGridView.AllowUserToDeleteRows = false;
            this.allItemsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allItemsGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allItemsGridView.Location = new System.Drawing.Point(0, 0);
            this.allItemsGridView.Name = "allItemsGridView";
            this.allItemsGridView.ReadOnly = true;
            this.allItemsGridView.Size = new System.Drawing.Size(877, 502);
            this.allItemsGridView.TabIndex = 0;
            // 
            // salesHistoryPage
            // 
            this.salesHistoryPage.Controls.Add(this.saleItemsGridView);
            this.salesHistoryPage.Controls.Add(this.salesHistoryGridView);
            this.salesHistoryPage.Location = new System.Drawing.Point(4, 22);
            this.salesHistoryPage.Name = "salesHistoryPage";
            this.salesHistoryPage.Size = new System.Drawing.Size(877, 502);
            this.salesHistoryPage.TabIndex = 3;
            this.salesHistoryPage.Text = "История продаж";
            this.salesHistoryPage.UseVisualStyleBackColor = true;
            // 
            // saleItemsGridView
            // 
            this.saleItemsGridView.AllowUserToAddRows = false;
            this.saleItemsGridView.AllowUserToDeleteRows = false;
            this.saleItemsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.saleItemsGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saleItemsGridView.Location = new System.Drawing.Point(565, 0);
            this.saleItemsGridView.Name = "saleItemsGridView";
            this.saleItemsGridView.ReadOnly = true;
            this.saleItemsGridView.Size = new System.Drawing.Size(312, 502);
            this.saleItemsGridView.TabIndex = 1;
            // 
            // salesHistoryGridView
            // 
            this.salesHistoryGridView.AllowUserToAddRows = false;
            this.salesHistoryGridView.AllowUserToDeleteRows = false;
            this.salesHistoryGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.salesHistoryGridView.Dock = System.Windows.Forms.DockStyle.Left;
            this.salesHistoryGridView.Location = new System.Drawing.Point(0, 0);
            this.salesHistoryGridView.Name = "salesHistoryGridView";
            this.salesHistoryGridView.ReadOnly = true;
            this.salesHistoryGridView.Size = new System.Drawing.Size(565, 502);
            this.salesHistoryGridView.TabIndex = 0;
            this.salesHistoryGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.salesHistoryGridView_CellClick);
            // 
            // incomeHistoryPage
            // 
            this.incomeHistoryPage.Controls.Add(this.incomeHistoryGridView);
            this.incomeHistoryPage.Location = new System.Drawing.Point(4, 22);
            this.incomeHistoryPage.Name = "incomeHistoryPage";
            this.incomeHistoryPage.Size = new System.Drawing.Size(877, 502);
            this.incomeHistoryPage.TabIndex = 4;
            this.incomeHistoryPage.Text = "История поступлений";
            this.incomeHistoryPage.UseVisualStyleBackColor = true;
            // 
            // incomeHistoryGridView
            // 
            this.incomeHistoryGridView.AllowUserToAddRows = false;
            this.incomeHistoryGridView.AllowUserToDeleteRows = false;
            this.incomeHistoryGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.incomeHistoryGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incomeHistoryGridView.Location = new System.Drawing.Point(0, 0);
            this.incomeHistoryGridView.Name = "incomeHistoryGridView";
            this.incomeHistoryGridView.ReadOnly = true;
            this.incomeHistoryGridView.Size = new System.Drawing.Size(877, 502);
            this.incomeHistoryGridView.TabIndex = 0;
            // 
            // AdminMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 528);
            this.Controls.Add(this.tabControl1);
            this.Name = "AdminMainForm";
            this.Text = "Администратор";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminMainForm_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.usersListPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.usersGridView)).EndInit();
            this.addUserPage.ResumeLayout(false);
            this.addUserPage.PerformLayout();
            this.itemsListPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.allItemsGridView)).EndInit();
            this.salesHistoryPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.saleItemsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesHistoryGridView)).EndInit();
            this.incomeHistoryPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incomeHistoryGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage usersListPage;
        private System.Windows.Forms.TabPage addUserPage;
        private System.Windows.Forms.TabPage itemsListPage;
        private System.Windows.Forms.TabPage salesHistoryPage;
        private System.Windows.Forms.TabPage incomeHistoryPage;
        private System.Windows.Forms.DataGridView usersGridView;
        private System.Windows.Forms.Label newUserPasswordLabel;
        private System.Windows.Forms.Label newUserSurnameLabel;
        private System.Windows.Forms.Label newUserNameLabel;
        private System.Windows.Forms.Label newUserLoginLabel;
        private System.Windows.Forms.Label newUserChooseRoleLabel;
        private System.Windows.Forms.ComboBox newUserChooseRoleDropDown;
        private System.Windows.Forms.TextBox newUserPasswordTextBox;
        private System.Windows.Forms.TextBox newUserSurnameTextBox;
        private System.Windows.Forms.TextBox newUserNameTextBox;
        private System.Windows.Forms.TextBox newUserLoginTextBox;
        private System.Windows.Forms.Button addNewUserButton;
        private System.Windows.Forms.DataGridView allItemsGridView;
        private System.Windows.Forms.DataGridView salesHistoryGridView;
        private System.Windows.Forms.DataGridView incomeHistoryGridView;
        private System.Windows.Forms.DataGridView saleItemsGridView;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}