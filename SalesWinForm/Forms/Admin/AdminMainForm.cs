﻿using SalesWinForm.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWinForm.Forms.Admin
{
    public partial class AdminMainForm : Form
    {
        public AdminMainForm(UserAuthData userAuthData)
        {
            InitializeComponent();
            AdminHandler.FillGridViewWithUsersData(ref usersGridView);
            newUserChooseRoleDropDown.Items.AddRange(AdminHandler.GetRolesAsDropDown());
            allItemsGridView.DataSource = AdminHandler.GetAllItemsAsDataTable();
            salesHistoryGridView.DataSource = AdminHandler.GetAllSalesAsDataTable();
            incomeHistoryGridView.DataSource = AdminHandler.GetAllIncomeAsDataTable();
        }

        private void addNewUserButton_Click(object sender, EventArgs e)
        {
            if ((newUserChooseRoleDropDown.SelectedItem != null)
                && newUserLoginTextBox.Text.Length > 3
                && newUserNameTextBox.Text.Length > 3
                && newUserSurnameTextBox.Text.Length > 3)
            {
                using (sales_dbEntities db = new sales_dbEntities())
                {
                    users newUser = new users();
                    newUser.active = true;
                    newUser.login = newUserLoginTextBox.Text;
                    newUser.name = newUserNameTextBox.Text;
                    newUser.surname = newUserSurnameTextBox.Text;
                    newUser.fk_role = ((DropDownData)this.newUserChooseRoleDropDown.SelectedItem).Value;
                    string uid = AuthentificationHandler.GenerateUid();
                    newUser.uid = uid;
                    newUser.password = AuthentificationHandler.EncodePassword(newUserPasswordTextBox.Text, uid);
                    db.users.Add(newUser);
                    db.SaveChanges();
                }
            }
            AdminHandler.FillGridViewWithUsersData(ref usersGridView);
        }

        private void salesHistoryGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            DataGridViewRow row = salesHistoryGridView.Rows[rowIndex];
            int saleId = (int)row.Cells[0].Value;
            saleItemsGridView.DataSource = AdminHandler.GetAllSalesItemAsDataTable(saleId);
        }

        private void AdminMainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
