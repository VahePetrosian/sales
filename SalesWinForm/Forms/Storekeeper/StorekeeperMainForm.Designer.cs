﻿namespace SalesWinForm.Forms.Storekeeper
{
    partial class StorekeeperMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.allIncomesPage = new System.Windows.Forms.TabPage();
            this.allIncomesGridView = new System.Windows.Forms.DataGridView();
            this.existingItemIncome = new System.Windows.Forms.TabPage();
            this.existingItemCountTextBox = new System.Windows.Forms.TextBox();
            this.existingItemDropDown = new System.Windows.Forms.ComboBox();
            this.existingItemCountLabel = new System.Windows.Forms.Label();
            this.eistingItemChooseLabel = new System.Windows.Forms.Label();
            this.existingItemAddButton = new System.Windows.Forms.Button();
            this.newItemIncome = new System.Windows.Forms.TabPage();
            this.newItemLocationLabel = new System.Windows.Forms.Label();
            this.newItemPriceLabel = new System.Windows.Forms.Label();
            this.newItemCountLabel = new System.Windows.Forms.Label();
            this.newItemDescriptionLabel = new System.Windows.Forms.Label();
            this.newItemNameLabel = new System.Windows.Forms.Label();
            this.newItemAddButton = new System.Windows.Forms.Button();
            this.NewItemLocationTextBox = new System.Windows.Forms.TextBox();
            this.NewItemPriceTextBox = new System.Windows.Forms.TextBox();
            this.newItemCountTextBox = new System.Windows.Forms.TextBox();
            this.newItemDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.newItemNameTextBox = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.allIncomesPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allIncomesGridView)).BeginInit();
            this.existingItemIncome.SuspendLayout();
            this.newItemIncome.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.allIncomesPage);
            this.tabControl1.Controls.Add(this.existingItemIncome);
            this.tabControl1.Controls.Add(this.newItemIncome);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(684, 362);
            this.tabControl1.TabIndex = 0;
            // 
            // allIncomesPage
            // 
            this.allIncomesPage.Controls.Add(this.allIncomesGridView);
            this.allIncomesPage.Location = new System.Drawing.Point(4, 22);
            this.allIncomesPage.Name = "allIncomesPage";
            this.allIncomesPage.Padding = new System.Windows.Forms.Padding(3);
            this.allIncomesPage.Size = new System.Drawing.Size(676, 336);
            this.allIncomesPage.TabIndex = 0;
            this.allIncomesPage.Text = "Все поступления";
            this.allIncomesPage.UseVisualStyleBackColor = true;
            // 
            // allIncomesGridView
            // 
            this.allIncomesGridView.AllowUserToAddRows = false;
            this.allIncomesGridView.AllowUserToDeleteRows = false;
            this.allIncomesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allIncomesGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allIncomesGridView.Location = new System.Drawing.Point(3, 3);
            this.allIncomesGridView.Name = "allIncomesGridView";
            this.allIncomesGridView.ReadOnly = true;
            this.allIncomesGridView.Size = new System.Drawing.Size(670, 330);
            this.allIncomesGridView.TabIndex = 0;
            // 
            // existingItemIncome
            // 
            this.existingItemIncome.Controls.Add(this.existingItemCountTextBox);
            this.existingItemIncome.Controls.Add(this.existingItemDropDown);
            this.existingItemIncome.Controls.Add(this.existingItemCountLabel);
            this.existingItemIncome.Controls.Add(this.eistingItemChooseLabel);
            this.existingItemIncome.Controls.Add(this.existingItemAddButton);
            this.existingItemIncome.Location = new System.Drawing.Point(4, 22);
            this.existingItemIncome.Name = "existingItemIncome";
            this.existingItemIncome.Padding = new System.Windows.Forms.Padding(3);
            this.existingItemIncome.Size = new System.Drawing.Size(676, 336);
            this.existingItemIncome.TabIndex = 1;
            this.existingItemIncome.Text = "Поступление имеющегося товара";
            this.existingItemIncome.UseVisualStyleBackColor = true;
            // 
            // existingItemCountTextBox
            // 
            this.existingItemCountTextBox.Location = new System.Drawing.Point(71, 49);
            this.existingItemCountTextBox.Name = "existingItemCountTextBox";
            this.existingItemCountTextBox.Size = new System.Drawing.Size(121, 20);
            this.existingItemCountTextBox.TabIndex = 4;
            // 
            // existingItemDropDown
            // 
            this.existingItemDropDown.FormattingEnabled = true;
            this.existingItemDropDown.Location = new System.Drawing.Point(71, 15);
            this.existingItemDropDown.Name = "existingItemDropDown";
            this.existingItemDropDown.Size = new System.Drawing.Size(121, 21);
            this.existingItemDropDown.TabIndex = 3;
            // 
            // existingItemCountLabel
            // 
            this.existingItemCountLabel.AutoSize = true;
            this.existingItemCountLabel.Location = new System.Drawing.Point(19, 49);
            this.existingItemCountLabel.Name = "existingItemCountLabel";
            this.existingItemCountLabel.Size = new System.Drawing.Size(41, 13);
            this.existingItemCountLabel.TabIndex = 2;
            this.existingItemCountLabel.Text = "Кол-во";
            // 
            // eistingItemChooseLabel
            // 
            this.eistingItemChooseLabel.AutoSize = true;
            this.eistingItemChooseLabel.Location = new System.Drawing.Point(19, 18);
            this.eistingItemChooseLabel.Name = "eistingItemChooseLabel";
            this.eistingItemChooseLabel.Size = new System.Drawing.Size(38, 13);
            this.eistingItemChooseLabel.TabIndex = 1;
            this.eistingItemChooseLabel.Text = "Товар";
            // 
            // existingItemAddButton
            // 
            this.existingItemAddButton.Location = new System.Drawing.Point(22, 88);
            this.existingItemAddButton.Name = "existingItemAddButton";
            this.existingItemAddButton.Size = new System.Drawing.Size(170, 23);
            this.existingItemAddButton.TabIndex = 0;
            this.existingItemAddButton.Text = "Добавить";
            this.existingItemAddButton.UseVisualStyleBackColor = true;
            this.existingItemAddButton.Click += new System.EventHandler(this.existingItemAddButton_Click);
            // 
            // newItemIncome
            // 
            this.newItemIncome.Controls.Add(this.newItemLocationLabel);
            this.newItemIncome.Controls.Add(this.newItemPriceLabel);
            this.newItemIncome.Controls.Add(this.newItemCountLabel);
            this.newItemIncome.Controls.Add(this.newItemDescriptionLabel);
            this.newItemIncome.Controls.Add(this.newItemNameLabel);
            this.newItemIncome.Controls.Add(this.newItemAddButton);
            this.newItemIncome.Controls.Add(this.NewItemLocationTextBox);
            this.newItemIncome.Controls.Add(this.NewItemPriceTextBox);
            this.newItemIncome.Controls.Add(this.newItemCountTextBox);
            this.newItemIncome.Controls.Add(this.newItemDescriptionTextBox);
            this.newItemIncome.Controls.Add(this.newItemNameTextBox);
            this.newItemIncome.Location = new System.Drawing.Point(4, 22);
            this.newItemIncome.Name = "newItemIncome";
            this.newItemIncome.Size = new System.Drawing.Size(676, 336);
            this.newItemIncome.TabIndex = 2;
            this.newItemIncome.Text = "Поступление нового товара";
            this.newItemIncome.UseVisualStyleBackColor = true;
            // 
            // newItemLocationLabel
            // 
            this.newItemLocationLabel.AutoSize = true;
            this.newItemLocationLabel.Location = new System.Drawing.Point(8, 115);
            this.newItemLocationLabel.Name = "newItemLocationLabel";
            this.newItemLocationLabel.Size = new System.Drawing.Size(95, 13);
            this.newItemLocationLabel.TabIndex = 10;
            this.newItemLocationLabel.Text = "Местоположение";
            // 
            // newItemPriceLabel
            // 
            this.newItemPriceLabel.AutoSize = true;
            this.newItemPriceLabel.Location = new System.Drawing.Point(8, 88);
            this.newItemPriceLabel.Name = "newItemPriceLabel";
            this.newItemPriceLabel.Size = new System.Drawing.Size(33, 13);
            this.newItemPriceLabel.TabIndex = 9;
            this.newItemPriceLabel.Text = "Цена";
            // 
            // newItemCountLabel
            // 
            this.newItemCountLabel.AutoSize = true;
            this.newItemCountLabel.Location = new System.Drawing.Point(8, 61);
            this.newItemCountLabel.Name = "newItemCountLabel";
            this.newItemCountLabel.Size = new System.Drawing.Size(41, 13);
            this.newItemCountLabel.TabIndex = 8;
            this.newItemCountLabel.Text = "Кол-во";
            // 
            // newItemDescriptionLabel
            // 
            this.newItemDescriptionLabel.AutoSize = true;
            this.newItemDescriptionLabel.Location = new System.Drawing.Point(9, 34);
            this.newItemDescriptionLabel.Name = "newItemDescriptionLabel";
            this.newItemDescriptionLabel.Size = new System.Drawing.Size(57, 13);
            this.newItemDescriptionLabel.TabIndex = 7;
            this.newItemDescriptionLabel.Text = "Описание";
            // 
            // newItemNameLabel
            // 
            this.newItemNameLabel.AutoSize = true;
            this.newItemNameLabel.Location = new System.Drawing.Point(8, 7);
            this.newItemNameLabel.Name = "newItemNameLabel";
            this.newItemNameLabel.Size = new System.Drawing.Size(57, 13);
            this.newItemNameLabel.TabIndex = 6;
            this.newItemNameLabel.Text = "Название";
            // 
            // newItemAddButton
            // 
            this.newItemAddButton.Location = new System.Drawing.Point(8, 138);
            this.newItemAddButton.Name = "newItemAddButton";
            this.newItemAddButton.Size = new System.Drawing.Size(264, 23);
            this.newItemAddButton.TabIndex = 5;
            this.newItemAddButton.Text = "Добавить";
            this.newItemAddButton.UseVisualStyleBackColor = true;
            this.newItemAddButton.Click += new System.EventHandler(this.newItemAddButton_Click);
            // 
            // NewItemLocationTextBox
            // 
            this.NewItemLocationTextBox.Location = new System.Drawing.Point(115, 112);
            this.NewItemLocationTextBox.Name = "NewItemLocationTextBox";
            this.NewItemLocationTextBox.Size = new System.Drawing.Size(157, 20);
            this.NewItemLocationTextBox.TabIndex = 4;
            // 
            // NewItemPriceTextBox
            // 
            this.NewItemPriceTextBox.Location = new System.Drawing.Point(115, 85);
            this.NewItemPriceTextBox.Name = "NewItemPriceTextBox";
            this.NewItemPriceTextBox.Size = new System.Drawing.Size(157, 20);
            this.NewItemPriceTextBox.TabIndex = 3;
            // 
            // newItemCountTextBox
            // 
            this.newItemCountTextBox.Location = new System.Drawing.Point(115, 58);
            this.newItemCountTextBox.Name = "newItemCountTextBox";
            this.newItemCountTextBox.Size = new System.Drawing.Size(157, 20);
            this.newItemCountTextBox.TabIndex = 2;
            // 
            // newItemDescriptionTextBox
            // 
            this.newItemDescriptionTextBox.Location = new System.Drawing.Point(115, 31);
            this.newItemDescriptionTextBox.Name = "newItemDescriptionTextBox";
            this.newItemDescriptionTextBox.Size = new System.Drawing.Size(157, 20);
            this.newItemDescriptionTextBox.TabIndex = 1;
            // 
            // newItemNameTextBox
            // 
            this.newItemNameTextBox.Location = new System.Drawing.Point(115, 4);
            this.newItemNameTextBox.Name = "newItemNameTextBox";
            this.newItemNameTextBox.Size = new System.Drawing.Size(157, 20);
            this.newItemNameTextBox.TabIndex = 0;
            // 
            // StorekeeperMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.tabControl1);
            this.Name = "StorekeeperMainForm";
            this.Text = "Кладовщик";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StorekeeperMainForm_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.allIncomesPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.allIncomesGridView)).EndInit();
            this.existingItemIncome.ResumeLayout(false);
            this.existingItemIncome.PerformLayout();
            this.newItemIncome.ResumeLayout(false);
            this.newItemIncome.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage allIncomesPage;
        private System.Windows.Forms.DataGridView allIncomesGridView;
        private System.Windows.Forms.TabPage existingItemIncome;
        private System.Windows.Forms.TextBox existingItemCountTextBox;
        private System.Windows.Forms.ComboBox existingItemDropDown;
        private System.Windows.Forms.Label existingItemCountLabel;
        private System.Windows.Forms.Label eistingItemChooseLabel;
        private System.Windows.Forms.Button existingItemAddButton;
        private System.Windows.Forms.TabPage newItemIncome;
        private System.Windows.Forms.Label newItemNameLabel;
        private System.Windows.Forms.Button newItemAddButton;
        private System.Windows.Forms.TextBox NewItemLocationTextBox;
        private System.Windows.Forms.TextBox NewItemPriceTextBox;
        private System.Windows.Forms.TextBox newItemCountTextBox;
        private System.Windows.Forms.TextBox newItemDescriptionTextBox;
        private System.Windows.Forms.TextBox newItemNameTextBox;
        private System.Windows.Forms.Label newItemLocationLabel;
        private System.Windows.Forms.Label newItemPriceLabel;
        private System.Windows.Forms.Label newItemCountLabel;
        private System.Windows.Forms.Label newItemDescriptionLabel;
    }
}