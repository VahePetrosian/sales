﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SalesWinForm.Code;

namespace SalesWinForm.Forms.Storekeeper
{
    public partial class StorekeeperMainForm : Form
    {
        UserAuthData thisUser;

        public StorekeeperMainForm(UserAuthData userAuthData)
        {
            thisUser = userAuthData;
            InitializeComponent();
            existingItemDropDown.Items.AddRange(StorekeeperHandler.GetAllItemsAsDropDown());
            allIncomesGridView.DataSource = AdminHandler.GetAllIncomeAsDataTable();
        }

        private void newItemAddButton_Click(object sender, EventArgs e)
        {
            int count = 0;
            Int32.TryParse(newItemCountTextBox.Text, out count);
            decimal price = 0;
            Decimal.TryParse(NewItemPriceTextBox.Text, out price);

            if (count > 0 && price > 0)
            {
                using (sales_dbEntities db = new sales_dbEntities())
                {
                    items newItem = new items();
                    newItem.name = newItemNameTextBox.Text;
                    newItem.description = newItemDescriptionTextBox.Text;
                    newItem.count = count;
                    newItem.location = NewItemLocationTextBox.Text;
                    newItem.price = price;

                    db.items.Add(newItem);
                    db.SaveChanges();

                    items_income newIncome = new items_income();
                    newIncome.fk_user = thisUser.UserId;
                    newIncome.fk_item = newItem.id;
                    newIncome.count = count;
                    newIncome.date = DateTime.Now;

                    db.items_income.Add(newIncome);
                    db.SaveChanges();
                }
            }
            existingItemDropDown.Items.Clear();
            existingItemDropDown.Items.AddRange(StorekeeperHandler.GetAllItemsAsDropDown());
            existingItemDropDown.SelectedItem = null;
            allIncomesGridView.DataSource = AdminHandler.GetAllIncomeAsDataTable();
        }

        private void existingItemAddButton_Click(object sender, EventArgs e)
        {
            int count = 0;
            Int32.TryParse(existingItemCountTextBox.Text, out count);
            if (((DropDownData)this.existingItemDropDown.SelectedItem != null))
                {
                int itemId = ((DropDownData)this.existingItemDropDown.SelectedItem).Value;
                if (count > 0 && itemId > 0)
                {
                    using (sales_dbEntities db = new sales_dbEntities())
                    {
                        items_income newIncome = new items_income();
                        newIncome.count = count;
                        newIncome.date = DateTime.Now;
                        newIncome.fk_item = itemId;
                        newIncome.fk_user = thisUser.UserId;
                        db.items_income.Add(newIncome);
                        items thisItem = db.items.FirstOrDefault(item => item.id == itemId);
                        thisItem.count += count;
                        db.SaveChanges();
                    }
                }
            }
            // existingItemDropDown.Items.Clear();
            // existingItemDropDown.Items.AddRange(StorekeeperHandler.GetAllItemsAsDropDown());
            allIncomesGridView.DataSource = AdminHandler.GetAllIncomeAsDataTable();
        }

        private void StorekeeperMainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

    }
}
