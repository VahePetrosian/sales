﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SalesWinForm.Code;


namespace SalesWinForm
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            AuthentificationHandler authHandler = new AuthentificationHandler();
            string login = LoginTextBox.Text;
            string password = PasswordTextBox.Text;
            if (authHandler.CheckUserAuthData(login, password))
            {
                UserAuthData userAuthData = authHandler.GetUserAuthData(login);
                if (userAuthData == null)
                {
                    //log
                }
                switch (userAuthData.RoleCode)
                {
                    case RoleTypes.ADMIN:
                        Forms.Admin.AdminMainForm adminMainForm =
                            new Forms.Admin.AdminMainForm(userAuthData);
                        this.Hide();
                        adminMainForm.ShowDialog();

                        break;
                    case RoleTypes.CASHIER:
                        Forms.Cashier.CashierMainForm cashierMainForm =
                           new Forms.Cashier.CashierMainForm(userAuthData);
                        this.Hide();
                        cashierMainForm.ShowDialog();
                        break;
                    case RoleTypes.STOREKEEPER:
                        Forms.Storekeeper.StorekeeperMainForm storekeeperMainForm =
                           new Forms.Storekeeper.StorekeeperMainForm(userAuthData);
                        this.Hide();
                        storekeeperMainForm.ShowDialog();
                        break;
                    default:
                        //log
                        break;
                }
            }
            else
            {
                LoginErrorLabel.Visible = true;
            }
        }
    }
}
