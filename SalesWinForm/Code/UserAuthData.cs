﻿namespace SalesWinForm.Code
{
    public class UserAuthData
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string RoleCode { get; set; }
    }
}
