﻿namespace SalesWinForm.Code
{
    class CartDataModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Sum { get { return Price * Count; } private set { } }
        public decimal Price { get; set; }
        public int Count { get; set; }
    }
}
