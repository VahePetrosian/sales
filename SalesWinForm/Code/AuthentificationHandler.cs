﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesWinForm.Code
{
    class AuthentificationHandler
    {
        private sales_dbEntities db = new sales_dbEntities();

        public UserAuthData GetUserAuthData(string login) // login is unique
        {
            return (from usr in db.users
                    join role in db.roles
                    on usr.fk_role equals role.id
                    where usr.active == true
                    && role.active == true
                    && usr.login== login
                    select new UserAuthData
                    {
                        UserId = usr.id,
                        Name = usr.name,
                        Surname = usr.surname,
                        RoleCode = role.code
                    }).FirstOrDefault();
        }

        public bool CheckUserAuthData(string login, string password)
        {

            if (login == null || password == null)
                return false;
            if (login.Length < 4 || password.Length < 4)
                return false;
            users user = db.users.FirstOrDefault(usr => usr.active == true && usr.login == login);
            if (user == null)
                return false;// login not found
            if (user.password != EncodePassword(password, user.uid))
                return false;// wrong password
            return true;
        }

        public static string EncodePassword(string password, string uid)
        {
            string tmp = password + uid;
            return HashSHA1(tmp);
        }

        private static string HashSHA1(string value)
        {
            var sha1 = System.Security.Cryptography.SHA1.Create();
            var inputBytes = Encoding.ASCII.GetBytes(value);
            var hash = sha1.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static string GenerateUid()
        {
            return System.Guid.NewGuid().ToString();
        }
    }
}
