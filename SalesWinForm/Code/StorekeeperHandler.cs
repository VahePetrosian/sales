﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesWinForm.Code
{
    class StorekeeperHandler
    {
        public static DropDownData[] GetAllItemsAsDropDown()
        {
            DropDownData[] dropDownDataList;
            using (sales_dbEntities db = new sales_dbEntities())
            {
                dropDownDataList = db.items.
                    Select(item => new DropDownData() { Value = item.id, Text = item.name }).ToArray();
            }
            return dropDownDataList;
        }
    }
}
