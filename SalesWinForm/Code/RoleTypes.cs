﻿namespace SalesWinForm.Code
{
    class RoleTypes
    {
        public const string STOREKEEPER = "storekeeper";
        public const string ADMIN = "admin";
        public const string CASHIER = "cashier";
    }
}
