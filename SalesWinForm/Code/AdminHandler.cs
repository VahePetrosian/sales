﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWinForm.Code
{
    class AdminHandler
    {
        public static DropDownData[] GetRolesAsDropDown()
        {
            DropDownData[] dropDownDataList;
            using (sales_dbEntities db = new sales_dbEntities())
            {
                dropDownDataList = db.roles.Where(role => role.active == true)
                .Select(role => new DropDownData() { Value = role.id, Text = role.name }).ToArray();
            }
            return dropDownDataList;
        }

        public static void FillGridViewWithUsersData(ref DataGridView gridView)
        {
            sales_dbEntities db = new sales_dbEntities();
            List<UserAuthData> allActiveUsers = (from usr in db.users
                                                 join role in db.roles
                                                 on usr.fk_role equals role.id
                                                 where usr.active == true
                                                 && role.active == true
                                                 select new UserAuthData()
                                                 {
                                                     Name = usr.name,
                                                     Surname = usr.surname,
                                                     RoleCode = role.code,
                                                     UserId = usr.id
                                                 }).ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Surname", typeof(string));
            dt.Columns.Add("Role", typeof(string));

            dt.Columns[0].ColumnName = "ID";
            dt.Columns[1].ColumnName = "Имя";
            dt.Columns[2].ColumnName = "Фамилия";
            dt.Columns[3].ColumnName = "Роль";

            foreach (UserAuthData currentUserData in allActiveUsers)
            {
                dt.Rows.Add(currentUserData.UserId,
                    currentUserData.Name,
                    currentUserData.Surname,
                    currentUserData.RoleCode);
            }

            gridView.DataSource = dt;

        }

        public static DataTable GetAllItemsAsDataTable()
        {
            sales_dbEntities db = new sales_dbEntities();
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("Price", typeof(string));
            dataTable.Columns.Add("Count", typeof(int));
            dataTable.Columns.Add("Location", typeof(string));

            dataTable.Columns[0].ColumnName = "ID";
            dataTable.Columns[1].ColumnName = "Название";
            dataTable.Columns[2].ColumnName = "Описание";
            dataTable.Columns[3].ColumnName = "Цена";
            dataTable.Columns[4].ColumnName = "Количество";
            dataTable.Columns[5].ColumnName = "Местоположение";

            List<items> allAvailibleItems = db.items.ToList();
            foreach (items i in allAvailibleItems)
            {
                dataTable.Rows.Add(i.id, i.name, i.description,
                    i.price.ToString("0.##"), i.count, i.location);
            }
            return dataTable;
        }

        public static DataTable GetAllSalesAsDataTable()
        {
            sales_dbEntities db = new sales_dbEntities();
            DataTable dataTable = new DataTable();

            List<sales> allSales = db.sales.OrderByDescending(sale => sale.date).ToList();
            List<users> allUsers = db.users.ToList();

            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("User", typeof(string));
            dataTable.Columns.Add("Cost", typeof(string));
            dataTable.Columns.Add("Cash", typeof(string));
            dataTable.Columns.Add("Change", typeof(string));
            dataTable.Columns.Add("Date", typeof(string));

            dataTable.Columns[0].ColumnName = "ID";
            dataTable.Columns[1].ColumnName = "Пользователь";
            dataTable.Columns[2].ColumnName = "Стоимость";
            dataTable.Columns[3].ColumnName = "Наличные";
            dataTable.Columns[4].ColumnName = "Сдача";
            dataTable.Columns[5].ColumnName = "Дата/Время";

            foreach (sales sale in allSales)
            {
                dataTable.Rows.Add(
                    sale.id,
                    allUsers.Where(usr => usr.id == sale.fk_user).Select(usr => usr.surname + " " + usr.name).FirstOrDefault(),
                    sale.cost.ToString("0.##"),
                    sale.cash.ToString("0.##"),
                    sale.change.ToString("0.##"),
                    sale.date.ToString());
            }

            return dataTable;
        }

        public static DataTable GetAllSalesItemAsDataTable(int saleId)
        {
            sales_dbEntities db = new sales_dbEntities();
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("ItemPrice", typeof(string));
            dataTable.Columns.Add("Count", typeof(int));
            dataTable.Columns.Add("TotalPrice", typeof(string));

            dataTable.Columns[0].ColumnName = "ID";
            dataTable.Columns[1].ColumnName = "Название";
            dataTable.Columns[2].ColumnName = "Описание";
            dataTable.Columns[3].ColumnName = "Цена за 1";
            dataTable.Columns[4].ColumnName = "Кол-во";
            dataTable.Columns[5].ColumnName = "Цена";

            List<items> allItems = db.items.ToList();
            List<sales_items> allSaleItems = db.sales_items.Where(si => si.fk_sale == saleId).ToList();
            foreach (sales_items sale_item in allSaleItems)
            {
                items item = allItems.FirstOrDefault(it => it.id == sale_item.fk_item);
                dataTable.Rows.Add(
                    sale_item.id,
                    item.name,
                    item.description,
                    item.price,
                    sale_item.count,
                    (item.price * sale_item.count).ToString("0.##"));
            }

            return dataTable;
        }

        public static DataTable GetAllIncomeAsDataTable()
        {
            sales_dbEntities db = new sales_dbEntities();
            DataTable dataTable = new DataTable();

            List<items_income> allIncomes = db.items_income.OrderByDescending(income => income.date).ToList();
            List<items> allItems = db.items.ToList();
            List<users> allUsers = db.users.ToList();

            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("User", typeof(string));
            dataTable.Columns.Add("Item", typeof(string));
            dataTable.Columns.Add("ItemDescription", typeof(string));
            dataTable.Columns.Add("Price", typeof(string));
            dataTable.Columns.Add("Count", typeof(int));
            dataTable.Columns.Add("Date", typeof(string));

            dataTable.Columns[0].ColumnName = "ID";
            dataTable.Columns[1].ColumnName = "Пользователь";
            dataTable.Columns[2].ColumnName = "Название";
            dataTable.Columns[3].ColumnName = "Описание";
            dataTable.Columns[4].ColumnName = "Цена";
            dataTable.Columns[5].ColumnName = "Кол-во";
            dataTable.Columns[6].ColumnName = "Дата/Время";

            foreach (items_income income in allIncomes)
            {
                items item = allItems.FirstOrDefault(it => it.id == income.fk_item);
                dataTable.Rows.Add(
                    income.id,
                    allUsers.Where(usr => usr.id == income.fk_user).Select(usr => usr.surname + " " + usr.name).FirstOrDefault(),
                  item.name,
                  item.description,
                    item.price.ToString("0.##"),
                    income.count,
                    income.date.ToString());
            }

            return dataTable;
        }
    }
}
