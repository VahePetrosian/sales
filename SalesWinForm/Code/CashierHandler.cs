﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesWinForm.Code
{
    class CashierHandler
    {
        public static DataTable GetSearchResultDataTable(string searchValue)
        {
            sales_dbEntities db = new sales_dbEntities();
            DataTable dataTable = new DataTable();
            List<items> searchResultItems = (from item in db.items
                                             where item.count > 0
                                             && item.name.Contains(searchValue)
                                             select item).ToList();

            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("Price", typeof(string));
            dataTable.Columns.Add("Count", typeof(int));

            dataTable.Columns[0].ColumnName = "ID";
            dataTable.Columns[1].ColumnName = "Название";
            dataTable.Columns[2].ColumnName = "Описание";
            dataTable.Columns[3].ColumnName = "Цена";
            dataTable.Columns[4].ColumnName = "Кол-во";

            foreach (items item in searchResultItems)
            {
                dataTable.Rows.Add(
                    item.id,
                    item.name,
                    item.description,
                    item.price.ToString("0.##"),
                    item.count);
            }

            return dataTable;
        }

        public static DataTable GetCardDataTable(List<CartDataModel> cartList)
        {
            DataTable dataTable = new DataTable();
            dataTable = new DataTable();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Count", typeof(int));
            dataTable.Columns.Add("Sum", typeof(string));

            dataTable.Columns[0].ColumnName = "Код";
            dataTable.Columns[1].ColumnName = "Название";
            dataTable.Columns[2].ColumnName = "Кол-во";
            dataTable.Columns[3].ColumnName = "Сумма";

            cartList.ForEach(item => dataTable.Rows.Add(item.Id, item.Name, item.Count, item.Sum.ToString("0.##")));

            return dataTable;
        }

        public static int AddItemToCart(int itemId, int count, ref List<CartDataModel> cart)// return status
        {
            sales_dbEntities db = new sales_dbEntities();
            items item = db.items.FirstOrDefault(itm => itm.id == itemId);
            CartDataModel existingItem = cart.FirstOrDefault(itm => itm.Id == itemId);

            if (existingItem == null)
            {
                if (count > item.count)
                    return 2;// not enough items in store
                cart.Add(new CartDataModel { Id = item.id, Count = count, Description = item.description, Name = item.name, Price = item.price });
            }
            else
            {
                if ((count + existingItem.Count) > item.count)
                    return 2;// not enough items in store
                existingItem.Count += count;
            }
            return 1; //success
        }

        public static void RemoveItemFromCart(int itemId, ref List<CartDataModel> cart)// return status
        {
            CartDataModel itemToRemove = cart.FirstOrDefault(itm => itm.Id == itemId);
            cart.Remove(itemToRemove);

        }

        public static void SaveCartToDb(List<CartDataModel> cart, decimal cash, decimal total, int userId)
        {
            sales_dbEntities db = new sales_dbEntities();
            sales newSale = new sales();
            newSale.cash = cash;
            newSale.cost = total;
            newSale.change = cash - total;
            newSale.fk_user = userId;
            newSale.date = DateTime.Now;
            db.sales.Add(newSale);
            db.SaveChanges();

            List<items> allItems = db.items.ToList();
            foreach (CartDataModel item in cart)
            {
                sales_items newSaleItem = new sales_items();
                newSaleItem.fk_sale = newSale.id;
                newSaleItem.fk_item = item.Id;
                newSaleItem.count = item.Count;
                db.sales_items.Add(newSaleItem);

                allItems.Where(itm => itm.id == item.Id).FirstOrDefault().count -= item.Count;
            }
            db.SaveChanges();
        }

    }
}
