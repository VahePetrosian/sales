﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.Configuration;
using SalesMVC.Models;

namespace SalesMVC.Code
{
    public class AuthHandler
    {
        public static bool CheckUserPassword(users user, string enteredPassword)
        {
            string password = EncodePassword(enteredPassword, user.uid);
            return password.Equals(user.password);
        }
        private static string EncodePassword(string password, string uid)
        {
            string tmp = password + uid;
            return HashSHA1(tmp);
        }
        private static string HashSHA1(string value)
        {
            var sha1 = System.Security.Cryptography.SHA1.Create();
            var inputBytes = Encoding.ASCII.GetBytes(value);
            var hash = sha1.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        public static string GetAuthenticatedUserToken(FormsIdentity id)
        {
            string result = HashSHA1(id.Ticket.Name + id.Ticket.IssueDate);
            return result;
        }
        public static bool AuthenticateUser(HttpResponseBase Response, users user)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User.Identity is FormsIdentity)
            {
                string userToken = GetAuthenticatedUserToken((FormsIdentity)HttpContext.Current.User.Identity);
                HttpContext.Current.Cache.Remove(userToken);
            }
            string userCookieData = "";
            if (user?.id != null)
                userCookieData = user.id.ToString();
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, // Ticket version
                user.login, // Username associated with ticket
                DateTime.Now, // Date/time issued
                DateTime.Now.AddDays(1), // Date/time to expire
                true, // "true" for a persistent user cookie
                userCookieData, // User-data, in this case the roles
                FormsAuthentication.FormsCookiePath);// Path cookie valid for
            string hash = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, // Name of auth cookie
                                               hash); // Hashed ticket
            if (ticket.IsPersistent)
                cookie.Expires = ticket.Expiration;
            Response.Cookies.Add(cookie);
            return true;
        }
        public static bool SetUserFromCookie(FormsIdentity id, HttpResponseBase response)
        {
            string userToken = GetAuthenticatedUserToken(id);
            CashierPrincipal principal = (CashierPrincipal)HttpContext.Current.Cache.Get(userToken);
            if (principal == null)
            {
                principal = CreateCashierPrincipal(id);
                if (principal == null)
                {
                    LogoutUser(response);
                    return false;
                }
                HttpContext.Current.Cache.Add(
                        userToken,
                        principal,
                        null,
                        System.Web.Caching.Cache.NoAbsoluteExpiration,
                        new TimeSpan(0, 30, 0),
                        System.Web.Caching.CacheItemPriority.Default,
                        null);
            }
            HttpContext.Current.User = principal;
            return true;
        }
        private static CashierPrincipal CreateCashierPrincipal(FormsIdentity id)
        {

            sales_dbEntities db = new sales_dbEntities();
             AuthData authData = (from a in db.users
                                 where a.login == id.Ticket.Name
                                 select new AuthData() {Id = a.id,Name = a.name, Surname = a.surname }).FirstOrDefault();
            string[] userRoles = { "cashier", "authenticated" }; // user this if we need more user roles
            return new CashierPrincipal(id, userRoles, authData);
        }
        public static void KillAuth(HttpResponseBase Response)
        {
            FormsAuthentication.SignOut();

            if (HttpContext.Current.User.Identity is FormsIdentity)
            {
                FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;
                HttpContext.Current.Cache.Remove(id.Ticket.Name);
            }

            Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
        }

        public static bool LogoutUser(HttpResponseBase Response)
        {

            KillAuth(Response);
            // @source: https://stackoverflow.com/a/1306932
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);
            SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);
            Response.Clear();
            return true;
        }
    }
}