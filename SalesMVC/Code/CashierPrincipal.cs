﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using SalesMVC.Models;

namespace SalesMVC.Code
{
    public class CashierPrincipal : GenericPrincipal
    {
        public AuthData UserAuthData { get; set; }
        public CashierPrincipal(IIdentity identity, string[] roles, AuthData data) : base(identity, roles)
        {
            UserAuthData = data;
        }

    }
}