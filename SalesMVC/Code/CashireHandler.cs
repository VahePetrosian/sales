﻿using SalesMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesMVC.Code
{
    public class CashireHandler
    {
        public static List<SearchItemResultModel> SearchItems(string searchValue)
        {
            sales_dbEntities db = new sales_dbEntities();
            List<SearchItemResultModel> result = (from a in db.items
                                                  where a.name.Contains(searchValue)
                                                  && a.count >0
                                                  select new SearchItemResultModel()
                                                  {
                                                      Count = a.count,
                                                      ItemDescription = a.description,
                                                      ItemId = a.id,
                                                      ItemName = a.name
                                                  }).ToList();
            return result;
        }
    }
}
