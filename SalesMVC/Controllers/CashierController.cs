﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesMVC.Code;
using SalesMVC.Models;
namespace SalesMVC.Controllers
{
    public class CashierController : Controller
    {
        // GET: Cashier
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllCarts()
        {
            AuthData user = ((SalesMVC.Code.CashierPrincipal)User).UserAuthData;
            SalesHistoryModel model = new SalesHistoryModel(user.Id);
            return View(model);
        }

        public ActionResult NewCart()
        {
            CartModel model = new CartModel();
            return View(model);
        }
        public PartialViewResult SearchItem(string searchValue)
        {
            CartModel model = new CartModel();
            model.SearchResult = CashireHandler.SearchItems(searchValue);
            return PartialView("partial/SearchItem", model);
        }
        public PartialViewResult AddItem(int itemId, CartModel model)
        {
            if (model.AddItemCount > 0)
            {
                model.AddItem(itemId);
            }
            return PartialView("partial/CartItems", model);
        }


        public PartialViewResult DeleteItem(int itemId, CartModel model)
        {
            model.DeleteItem(itemId);
            return PartialView("partial/CartItems", model);
        }

        public ActionResult CartView(int saleId)
        {
            sales_dbEntities db = new sales_dbEntities();
            sales sale = db.sales.FirstOrDefault(mc => mc.id == saleId);
            CartHistoryModel model = new CartHistoryModel(sale);
            return View(model);
        }

        public ActionResult SaveNewSale(CartModel model)
        {
            if (model.ItemsInCart.Count() > 0)
            {
                if (model.Cash >= model.TotalSum)
                {
                    AuthData user = ((SalesMVC.Code.CashierPrincipal)User).UserAuthData;
                    sales_dbEntities db = new sales_dbEntities();
                    sales newSale = new sales();
                    newSale.fk_user = user.Id;
                    newSale.cash = model.Cash;
                    newSale.cost = model.TotalSum;
                    newSale.change = newSale.cash - newSale.cost;
                    newSale.date = DateTime.Now;
                    db.sales.Add(newSale);
                    db.SaveChanges();

                    foreach (CartItemModel cartItem in model.ItemsInCart)
                    {
                        sales_items si = new sales_items();
                        si.count = cartItem.Count;
                        si.fk_item = cartItem.ItemId;
                        si.fk_sale = newSale.id;
                        db.sales_items.Add(si);
                        items thisItem = db.items.FirstOrDefault(mc => mc.id == si.fk_item);
                        thisItem.count -= si.count;
                    }

                    db.SaveChanges();
                    return RedirectToAction("CartView", "Cashier", new { saleId = newSale.id });
                }
            }
            return RedirectToAction("NewCart");
        }
    }
}