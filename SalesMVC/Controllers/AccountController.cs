﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SalesMVC.Models;
using SalesMVC.Code;

namespace SalesMVC.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            if(User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Cashier");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                users user;
                roles role;
                using (sales_dbEntities db = new sales_dbEntities())
                {
                    user = db.users.Where(a => a.login.Equals(model.Login) && a.active == true).FirstOrDefault();

                    if (user == null)
                    {
                        ModelState.AddModelError("", "Логин или пароль указаны неверно!");
                    }
                    else
                    {
                        role = db.roles.FirstOrDefault(r => r.id == user.fk_role);

                        if (AuthHandler.CheckUserPassword(user, model.Password))
                        {
                            if (role.code == "cashier")
                            {
                                AuthHandler.AuthenticateUser(Response, user);
                                return RedirectToAction("Index", "Cashier");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Доступ только для кассиров!");
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Логин или пароль указаны неверно!");
                        }
                    }
                }
            }
            return View(model);
        }

        public RedirectToRouteResult Logout()
        {
            AuthHandler.LogoutUser(Response);
            return RedirectToAction("Login", "Account");
        }
    }
}