﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesMVC.Models
{
    public class SearchItemResultModel
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public int Count { get; set; }
    }
}