﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesMVC.Models
{
    public class CartItemHistoryModel
    {
        private sales_dbEntities db = new sales_dbEntities();

        public int SaleItemId { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }

        public CartItemHistoryModel(sales_items saleItem)
        {
            SaleItemId = saleItem.id;
            ItemId = saleItem.id;
            items item = db.items.FirstOrDefault(itm => itm.id == saleItem.fk_item);
            ItemName = item.name;
            ItemDescription = item.description;
            Count = saleItem.count;
            Price = item.price;
        }
    }
    public class CartHistoryModel
    {
        private sales_dbEntities db = new sales_dbEntities();

        public int SaleId { get; set; }
        public decimal Cost { get; set; }
        public decimal Cash { get; set; }
        public decimal Change { get; set; }
        public DateTime Date { get; set; }
        public List<CartItemHistoryModel> ItemsInCart { get; set; }

        public CartHistoryModel(sales sale)
        {
            SaleId = sale.id;
            Cost = sale.cost;
            Change = sale.change;
            Cash = sale.cash;
            Date = sale.date;
            List<sales_items> allItemsInThisSale = db.sales_items.Where(si => si.fk_sale == sale.id).ToList();
            ItemsInCart = new List<CartItemHistoryModel>();
            foreach (sales_items saleItem in allItemsInThisSale)
            {
                ItemsInCart.Add(new CartItemHistoryModel(saleItem));
            }
        }
    }
    public class SalesHistoryModel
    {
        private sales_dbEntities db = new sales_dbEntities();
        public List<CartHistoryModel> allCarts { get; set; }
        public SalesHistoryModel(int userId)
        {
            List<sales> allUserSales = db.sales.Where(s => s.fk_user == userId).OrderBy(s => s.date).ToList();
            allCarts = new List<CartHistoryModel>();
            foreach (sales sale in allUserSales)
            {
                allCarts.Add(new CartHistoryModel(sale));
            }
        }

    }
}