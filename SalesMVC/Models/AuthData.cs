﻿namespace SalesMVC.Models
{
    public class AuthData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FIO
        {
            get
            {
                return Surname+" "+Name;
            }
        }
    }
}