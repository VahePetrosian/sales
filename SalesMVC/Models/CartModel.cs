﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesMVC.Models
{
    public class CartItemModel
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
        public decimal Sum { get { return Count * Price; } }
    }

    public class CartModel
    {
        private sales_dbEntities db = new sales_dbEntities();
        
        public decimal Cash { get; set; }
        public string SearchValue { get; set; }
        public int CartId { get; set; }
        public List<CartItemModel> ItemsInCart { get; set; }
        public decimal TotalSum { get { return ItemsInCart?.Sum(itm => itm.Sum) ?? 0; } }

        public int AddItemCount { get; set; }
        public List<SearchItemResultModel> SearchResult { get; set; }

        public CartModel()
        {
            SearchResult = new List<SearchItemResultModel>();
            ItemsInCart = new List<CartItemModel>();
        }

        private int GetMaxCountForItem(int itemId)
        {
            return db.items.FirstOrDefault(itm => itm.id == itemId).count;
        }
        public int AddItem(int itemId) //return status 1-ok 2-not_enough_on_stock
        {
            int count = AddItemCount;
            int maxCount = GetMaxCountForItem(itemId);
            CartItemModel existingItem = ItemsInCart.FirstOrDefault(itm => itm.ItemId == itemId);
            if (existingItem != null)
            {
                if ((existingItem.Count + count) > maxCount)
                    return 2;
                AddToExistingItem(itemId, count);
            }
            else
            {
                if (count > maxCount)
                    return 2;
                AddNewItem(itemId, count);
            }
            return 1;
        }
        private void AddNewItem(int itemId, int count)
        {
            CartItemModel newItem = new CartItemModel();
            newItem = (from a in db.items
                       where a.id == itemId
                       select new CartItemModel
                       {
                           Count = count,
                           ItemDescription = a.description,
                           ItemId = a.id,
                           ItemName = a.name,
                           Price = a.price
                       }).FirstOrDefault();
            ItemsInCart.Add(newItem);
        }
        private void AddToExistingItem(int itemId, int count)
        {
            CartItemModel item = ItemsInCart.Where(itm => itm.ItemId == itemId).FirstOrDefault();
            item.Count = item.Count += count;
        }
        public int DeleteItem(int itemId) //return status 1-ok 2-not_found
        {
            CartItemModel item = ItemsInCart.FirstOrDefault(mc => mc.ItemId == itemId);
            ItemsInCart.Remove(item);
            return 1;
        }
       
    }
}