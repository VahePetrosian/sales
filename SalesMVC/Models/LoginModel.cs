﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace SalesMVC.Models
{
    public class LoginModel
    {
        [DisplayName("Логин*:")]
        [UIHint("BootstrapTextbox")]
        [Required(ErrorMessage = "Поле должно быть установлено")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Длина логина должна быть от 4 до 20 символов")]
        public string Login { get; set; }

        [DisplayName("Пароль*:")]
        [UIHint("BootstrapPassword")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Длина пароля должна быть от 4 до 20 символов")]
        [Required(ErrorMessage = "Поле должно быть установлено")]      
        public string Password { get; set; }
    }
}