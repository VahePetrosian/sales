-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: salesdb
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(8000) DEFAULT NULL,
  `price` decimal(10,0) NOT NULL,
  `count` int(11) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Ручка','Шариковая синяя ручка',80,29,'Р4 П2 К8'),(2,'Карандаш','Графитовый карандаш В4',30,-276,'Р4 П2 К10'),(3,'Ластик','Ластик (стерка)',40,12,'Р4 П3 К1'),(4,'Штрих','Белый штрих',23,33,'Р3 П2 К3'),(5,'Фломастер','Синий фломастер',100,23,'Шкаф 1 полка 2 коробка 10');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_income`
--

DROP TABLE IF EXISTS `items_income`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_item` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `items_income_fk_items_idx` (`fk_item`),
  KEY `items_income_fk_user_idx` (`fk_user`),
  CONSTRAINT `items_income_fk_items` FOREIGN KEY (`fk_item`) REFERENCES `items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `items_income_fk_user` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_income`
--

LOCK TABLES `items_income` WRITE;
/*!40000 ALTER TABLE `items_income` DISABLE KEYS */;
INSERT INTO `items_income` VALUES (1,2,2,4,'2018-03-23 14:25:10'),(2,2,3,4,'2018-03-25 15:53:28'),(3,1,3,3,'2018-03-25 15:53:56'),(4,3,3,7,'2018-03-25 15:54:01'),(5,4,3,33,'2018-03-25 16:05:08'),(6,1,3,33,'2018-03-27 16:44:47'),(7,2,3,44,'2018-03-27 16:45:47'),(8,2,3,234,'2018-03-27 16:45:51'),(9,5,3,43,'2018-03-27 16:46:30');
/*!40000 ALTER TABLE `items_income` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,1,'Администратор','admin'),(2,1,'Кладовщик','storekeeper'),(3,1,'Кассир','cashier');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user` int(11) NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  `cash` decimal(10,0) NOT NULL,
  `change` decimal(10,0) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_fk_user_idx` (`fk_user`),
  CONSTRAINT `sales_fk_user` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (1,4,140,150,10,'2018-03-23 14:25:10'),(2,4,100,100,0,'2018-03-23 14:28:10'),(3,4,7680,22222,14542,'2018-03-26 00:02:24'),(9,4,90,100,10,'2018-03-27 11:05:11'),(10,4,2520,3000,480,'2018-03-27 11:05:52'),(11,4,120,200,80,'2018-03-27 12:13:19'),(12,4,90,100,10,'2018-03-27 13:32:52'),(13,4,0,200,200,'2018-03-27 13:51:30'),(14,4,160,100,-60,'2018-03-27 14:27:38'),(15,4,630,700,70,'2018-03-27 14:32:04'),(16,4,10550,12000,1450,'2018-03-27 16:49:07'),(17,4,9910,12000,2090,'2018-03-27 16:49:39');
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_items`
--

DROP TABLE IF EXISTS `sales_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_item` int(11) NOT NULL,
  `fk_sale` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_items_fk_item_idx` (`fk_item`),
  KEY `sales_items_fk_sale_idx` (`fk_sale`),
  CONSTRAINT `sales_items_fk_item` FOREIGN KEY (`fk_item`) REFERENCES `items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sales_items_fk_sale` FOREIGN KEY (`fk_sale`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_items`
--

LOCK TABLES `sales_items` WRITE;
/*!40000 ALTER TABLE `sales_items` DISABLE KEYS */;
INSERT INTO `sales_items` VALUES (1,1,1,4),(2,2,1,3),(3,3,1,6),(4,4,1,2),(5,4,2,5),(6,3,2,4),(7,1,3,5),(8,2,3,77),(9,2,9,3),(10,1,10,1),(11,2,10,48),(12,3,10,25),(13,2,11,4),(14,2,12,3),(15,1,14,2),(16,2,15,9),(17,3,15,9),(18,2,16,297),(19,3,16,8),(20,1,16,4),(21,5,16,10),(22,2,17,297),(23,5,17,10);
/*!40000 ALTER TABLE `sales_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `login` varchar(50) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `uid` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fk_role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_fk_role_idx` (`id`,`fk_role`),
  KEY `users_fk_role_idx1` (`fk_role`),
  CONSTRAINT `users_fk_role` FOREIGN KEY (`fk_role`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'usr0','Админ','Игорь','66a773d8-5dd7-4032-b38e-ec3929c6eac6','E5157489AD3D5A9AE546E975F2B87CC4EE23606A',1),(2,1,'usr1','Админ','Илья','31a06a96-4105-4bdc-aa3d-5e28692f24c6','5E5ABBB51409CE1E7A26EF13A43E79713A3B20B1',1),(3,1,'usr2','Кладовщик','Андрей','b3fad5d1-ee0a-4761-b0a7-674822cdde2e','734C9194A3CD59AB90258F411FCF964CFC3E5F04',2),(4,1,'usr3','Кассир','Николай','fccfe8e9-014b-4316-b548-24b1eddb0256','0814246FA1A7FFFCCE0D5EB995811DE10230A553',3),(5,1,'usr4','Кладовщик','Константин','b8ec7920-05d1-4b52-a049-b717a7ce36eb','04D56A8AF1E709B5C0C54817AB6A7AE9EBF719CD',2),(6,1,'usr5','Кассир','Владимир','b8ec7920-05d1-4b52-a049-b717a7ce36eb','04D56A8AF1E709B5C0C54817AB6A7AE9EBF719CD',3),(7,1,'usr6','Кассир','Валерия','3030287b-a87f-4d05-8882-059933b10b7e','B3E0938519D548501A3E9EDAEB93076857E80E99',3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-27 16:50:51
